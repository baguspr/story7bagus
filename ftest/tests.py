from django.test import TestCase, Client
from django.urls import resolve
from .views import index, confirm
from .models import Message

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class ftestUnitTest(TestCase):
    def test_ftest_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_ftest_confirm_url_is_exist(self):
        response = Client().get('/confirm/')
        self.assertEqual(response.status_code,200)

    def test_ftest_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_ftest_using_confirm_template(self):
        response = Client().get('/confirm/')
        self.assertTemplateUsed(response, 'confirm.html')

    def test_ftest_landing_page_contains_welcome(self):
        response = Client().get('/')
        self.assertContains(response, '<h1>WELCOME</h1>', status_code=200)
    
    def test_ftest_confirm_page_contains_confirm(self):
        response = Client().get('/confirm/')
        self.assertContains(response, '<h1>CONFIRM MESSAGE</h1>', status_code=200)

    def test_ftest_can_create_new_activity(self):
        #Creating a new message
        new_message = Message.objects.create(message='Hello World')

        #Retrieving all available message
        counting_all_available_message = Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)
    
    def test_ftest_can_create_name(self):
        #Creating a name
        name = Message.objects.create(name='Bagus')
    
        #Retrieving all available name
        counting_all_available_name = Message.objects.all().count()
        self.assertEqual(counting_all_available_name,1)

#FUNCTIONAL TEST

class ftestFuncTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(ftestFuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(ftestFuncTest, self).tearDown()

    def test_input_new_status(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        name = selenium.find_element_by_name('name')
        message = selenium.find_element_by_name('message')
        submit = selenium.find_element_by_name('submit')

        user_name = "Bagus"

        name.send_keys(user_name)
        message.send_keys('Hello')
        submit.send_keys(Keys.RETURN)

        time.sleep(5)

        submit_second = selenium.find_element_by_name('submit-button')
        submit_second.send_keys(Keys.RETURN)

        time.sleep(5)
        page = selenium.page_source
        self.assertIn(user_name, page)

	
