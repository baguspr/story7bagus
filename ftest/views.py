from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import MessageForm
from .models import Message

# Create your views here.

data_list = [{}]

def index(request):
	if request.method == 'POST':
		form = MessageForm(request.POST)
		data_list[0]["name"] = request.POST.get("name")
		data_list[0]["message"] = request.POST.get("message")

		if form.is_valid():
			return redirect("/confirm/")
		else:
			return render(request, 'homepage.html', {"data":Message.objects.all()})

	else:
			return render(request, 'homepage.html', {"data":Message.objects.all()})


def confirm(request):
	if request.method == 'POST':
		form = MessageForm(request.POST)
		form.save()
		return redirect("/")
	else:
		return render(request, 'confirm.html', {"data_list" : data_list})